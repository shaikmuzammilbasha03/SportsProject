package com.mvn.OnlineSportsAccessoriesStore.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mvn.OnlineSportsAccessoriesStore.entity.Product;
import com.mvn.OnlineSportsAccessoriesStore.repository.*;
@Service
public class ProductServiceImpl implements ProductService{
	
	@Autowired
	ProductRepository ProdRep;
	@Override
	public Product searchProd(int Id) {
		Product obj1 = ProdRep.findById(Id).get();
		return obj1;
	
	}
	@Override
	public List<Product> filterByCostHtoL(Collection<Product> products){
		List <Product> ls  = new ArrayList();
		 for(final Product product:products) {
			ls.add(product);
			 
		 }
		 Collections.sort(ls,new Comparator<Product>() {
			 public int compare(Product p1,Product p2) {
				 return p1.getCost()- p2.getCost();
			 }
		 });
		 return ls;
	
	}
	public List<Product> filterByCostLtoH(Collection<Product> products){
		List <Product> ls  = new ArrayList();
		 for(final Product product:products) {
			ls.add(product);
			 
		 }
		 Collections.sort(ls,Collections.reverseOrder());
		 return ls;
	}
}
