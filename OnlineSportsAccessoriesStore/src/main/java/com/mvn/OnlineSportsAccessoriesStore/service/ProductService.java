package com.mvn.OnlineSportsAccessoriesStore.service;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Service;

import com.mvn.OnlineSportsAccessoriesStore.entity.Product;

@Service
public interface ProductService {
	public Product searchProd(int Id);
	public List<Product> filterByCostHtoL(Collection<Product> products);
	public List<Product> filterByCostLtoH(Collection<Product> products);

}
